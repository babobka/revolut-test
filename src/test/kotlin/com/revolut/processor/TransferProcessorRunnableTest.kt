package com.revolut.processor

import com.github.kittinunf.result.Result
import com.nhaarman.mockito_kotlin.*
import com.revolut.model.PoisonTransaction
import com.revolut.model.TransferTransaction
import com.revolut.service.TransferService
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.LinkedBlockingDeque

class TransferProcessorRunnableTest {

    private val queue = LinkedBlockingDeque<TransferTransaction>()
    private val transferService = mock<TransferService>()
    private val processor = TransferProcessorRunnable(queue, transferService)

    @Before
    fun before() {
        reset(transferService)
        whenever(transferService.commitTransfer(any())).thenReturn(Result.of { })
        queue.clear()
    }

    /**
     * @given running processor with no actual transactions in queue
     * @when poison transaction occurs
     * @then no transaction is committed
     */
    @Test
    fun testNoTransactionsProcessed() {
        val processorThread = Thread(processor)
        processorThread.start()
        queue.put(PoisonTransaction())
        processorThread.join()
        verify(transferService, never()).commitTransfer(any())
    }

    /**
     * @given running processor with 10 transactions in queue
     * @when poison transaction occurs
     * @then 10 transactions are committed
     */
    @Test
    fun testTenTransactionsProcessed() {
        val dummyTransaction = TransferTransaction(
            UUID.randomUUID(), UUID.randomUUID(), BigDecimal.ONE
        )
        val transactions = 10
        val processorThread = Thread(processor)
        processorThread.start()
        for (i in 1..transactions) {
            queue.put(dummyTransaction)
        }
        queue.put(PoisonTransaction())
        processorThread.join()
        verify(transferService, times(transactions)).commitTransfer(any())
    }

    /**
     * @given running processor with 20 transactions in queue
     * @when poison transaction occurs after first 10 transactions
     * @then only first 10 transactions are committed
     */
    @Test
    fun testAvoidTransactionsAfterPoison() {
        val dummyTransaction = TransferTransaction(
            UUID.randomUUID(), UUID.randomUUID(), BigDecimal.ONE
        )
        val transactions = 10
        val processorThread = Thread(processor)
        processorThread.start()
        //Put ten transactions
        for (i in 1..transactions) {
            queue.put(dummyTransaction)
        }
        queue.put(PoisonTransaction())
        //Put more ten transactions after poisoning queue
        for (i in 1..transactions) {
            queue.put(dummyTransaction)
        }
        processorThread.join()
        verify(transferService, times(transactions)).commitTransfer(any())
    }
}
