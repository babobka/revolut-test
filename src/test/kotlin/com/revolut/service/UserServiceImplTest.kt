package com.revolut.service

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.revolut.model.User
import com.revolut.repository.UserRepository
import org.junit.Assert.*
import org.junit.Test
import java.math.BigDecimal
import java.util.*

class UserServiceImplTest {

    private val userRepository = mock<UserRepository>()
    private val userService = UserServiceImpl(userRepository)

    /**
     * @given id of not existing user
     * @when get() function is called using given id
     * @then IllegalAccessException is thrown
     */
    @Test
    fun testGetUserNotExisting() {
        val id = UUID.randomUUID()
        whenever(userRepository.get(id)).thenReturn(null)
        userService.getUser(id).fold({ fail() }, { ex -> assertTrue(ex is IllegalAccessException) })
    }

    /**
     * @given id of existing user
     * @when get() function is called using given id
     * @then get() returns some user
     */
    @Test
    fun testGetUserSuccess() {
        val id = UUID.randomUUID()
        val user = User(id, "test", "test@email.com", BigDecimal(0))
        whenever(userRepository.get(id)).thenReturn(user)
        userService.getUser(id).fold({ foundUser -> assertEquals(user, foundUser) }, {
            fail()
        })
    }
}
