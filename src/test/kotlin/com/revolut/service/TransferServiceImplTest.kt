package com.revolut.service

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.revolut.model.TransferTransaction
import com.revolut.model.User
import com.revolut.repository.UserRepository
import org.junit.Assert.*
import org.junit.Test
import java.math.BigDecimal
import java.util.*

class TransferServiceImplTest {

    private val userRepository = mock<UserRepository>()

    private val transferService = TransferServiceImpl(userRepository)

    /**
     * @given two user ids. first id user doesn't exist
     * @when first user sends money to second
     * @then IllegalAccessException is thrown
     */
    @Test
    fun testCommitTransferNotExistingFirstUsers() {
        val firstId = UUID.randomUUID()
        val secondId = UUID.randomUUID()
        whenever(userRepository.get(firstId)).thenReturn(null)
        whenever(userRepository.get(secondId)).thenReturn(User(secondId, "test", "test@email.com", BigDecimal(1)))
        val transferTransaction = TransferTransaction(firstId, secondId, BigDecimal(4))
        transferService.commitTransfer(transferTransaction)
            .fold({ fail() }, { ex -> assertTrue(ex is IllegalAccessException) })
    }

    /**
     * @given two user ids. second id user doesn't exist
     * @when first user sends money to second
     * @then IllegalAccessException is thrown
     */
    @Test
    fun testCommitTransferNotExistingSecondUsers() {
        val firstId = UUID.randomUUID()
        val secondId = UUID.randomUUID()
        whenever(userRepository.get(secondId)).thenReturn(null)
        whenever(userRepository.get(firstId)).thenReturn(User(secondId, "test", "test@email.com", BigDecimal(1)))
        val transferTransaction = TransferTransaction(firstId, secondId, BigDecimal(4))
        transferService.commitTransfer(transferTransaction)
            .fold({ fail() }, { ex -> assertTrue(ex is IllegalAccessException) })
    }

    /**
     * @given two user ids. first user send too much money to second user
     * @when first user sends money to second
     * @then IllegalStateException is thrown
     */
    @Test
    fun testCommitTransferNotEnoughMoney() {
        val firstId = UUID.randomUUID()
        val secondId = UUID.randomUUID()
        whenever(userRepository.get(firstId)).thenReturn(User(firstId, "test", "test@email.com", BigDecimal(1)))
        whenever(userRepository.get(secondId)).thenReturn(User(secondId, "test", "test@email.com", BigDecimal(1)))
        val transferTransaction = TransferTransaction(firstId, secondId, BigDecimal(4))
        transferService.commitTransfer(transferTransaction)
            .fold({ fail() }, { ex -> assertTrue(ex is IllegalStateException) })
    }

    /**
     * @given two user ids. first user has enough money to send
     * @when first user sends money to second
     * @then first user balance is decreased and second user balance is increased
     */
    @Test
    fun testCommitTransferSuccess() {
        val firstId = UUID.randomUUID()
        val secondId = UUID.randomUUID()
        val firstUser = User(firstId, "test", "test@email.com", BigDecimal(4))
        val secondUser = User(firstId, "test", "test@email.com", BigDecimal(1))
        whenever(userRepository.get(firstId)).thenReturn(firstUser)
        whenever(userRepository.get(secondId)).thenReturn(secondUser)
        val transferTransaction = TransferTransaction(firstId, secondId, firstUser.balance)
        transferService.commitTransfer(transferTransaction)
            .fold(
                {
                    assertEquals(BigDecimal.ZERO, firstUser.balance)
                    assertEquals(BigDecimal(5), secondUser.balance)
                    Unit
                },
                { fail() })
    }
}
