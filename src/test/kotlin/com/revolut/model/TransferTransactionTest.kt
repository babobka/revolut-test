package com.revolut.model

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.math.BigDecimal
import java.util.*

class TransferTransactionTest {

    /**
     * @given transaction with same ids
     * @when isValid() called
     * @then transaction is not considered valid
     */
    @Test
    fun testIsValidSameId() {
        val id = UUID.randomUUID()
        assertFalse(TransferTransaction(id, id, BigDecimal.ONE).isValid())
    }

    /**
     * @given transaction with negative amount
     * @when isValid() called
     * @then transaction is not considered valid
     */
    @Test
    fun testIsValidNegativeAmount() {
        assertFalse(TransferTransaction(UUID.randomUUID(), UUID.randomUUID(), BigDecimal.valueOf(-1)).isValid())
    }

    /**
     * @given transaction with zero amount
     * @when isValid() called
     * @then transaction is not considered valid
     */
    @Test
    fun testIsValidZeroAmount() {
        assertFalse(TransferTransaction(UUID.randomUUID(), UUID.randomUUID(), BigDecimal.ZERO).isValid())
    }

    /**
     * @given valid transaction
     * @when isValid() called
     * @then transaction is considered valid
     */
    @Test
    fun testIsValid() {
        assertTrue(TransferTransaction(UUID.randomUUID(), UUID.randomUUID(), BigDecimal.valueOf(0.5)).isValid())
    }

}
