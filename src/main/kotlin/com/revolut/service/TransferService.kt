package com.revolut.service

import com.github.kittinunf.result.Result
import com.revolut.model.TransferTransaction

/**
 * Service that is used to commit transfer transactions
 */
interface TransferService {
    /**
     * Commits transfer transactions
     * @param transferTransaction - transaction to commit
     * @return result of operation
     */
    fun commitTransfer(transferTransaction: TransferTransaction): Result<Unit, Exception>
}
