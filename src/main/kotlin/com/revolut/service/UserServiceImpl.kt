package com.revolut.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.revolut.model.User
import com.revolut.repository.UserRepository
import java.util.*

/**
 * User service implementation
 * @param userRepository - user repository
 */
class UserServiceImpl(
    private val userRepository: UserRepository
) : UserService {
    override fun getUser(id: UUID): Result<User, Exception> {
        return Result.of {
            //Get user from repository or throw exception if user is absent
            userRepository.get(id) ?: throw IllegalAccessException("User with id $id doesn't exist")
        }.map { user ->
            //Lock and return user
            user.lockUser {}
        }
    }
}
