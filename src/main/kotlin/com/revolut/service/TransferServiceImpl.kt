package com.revolut.service

import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.revolut.model.TransferTransaction
import com.revolut.model.User
import com.revolut.repository.UserRepository

/**
 * Transfer service implementation
 * @param userRepository - user repository
 */
class TransferServiceImpl(private val userRepository: UserRepository) : TransferService {

    override fun commitTransfer(transferTransaction: TransferTransaction): Result<Unit, Exception> {
        return Result.of {
            // Check if users exist
            val srcUser = userRepository.get(transferTransaction.srcId)
                ?: throw IllegalAccessException("User with id ${transferTransaction.srcId} doesn't exist")
            val destUser = userRepository.get(transferTransaction.destId)
                ?: throw IllegalAccessException("User with id ${transferTransaction.destId} doesn't exist")
            Pair(srcUser, destUser)
        }.map { pairOfUsers ->
            // Lock users to execute transaction
            val lockOrder = reorderLock(pairOfUsers)
            lockOrder.first.lockUser {
                lockOrder.second.lockUser {
                    // Check user has enough money to spend
                    if (pairOfUsers.first.balance < transferTransaction.amount) {
                        throw IllegalStateException("User ${pairOfUsers.first.name} doesn't have enough amount of money to execute withdrawal ($transferTransaction)")
                    }
                    // Execute deposit/withdrawal
                    pairOfUsers.first.balance -= transferTransaction.amount
                    pairOfUsers.second.balance += transferTransaction.amount
                }
            }
            Unit
        }
    }

    /**
     * Returns the order of locking.
     * If we lock users in the same order(bigger id first) then there will be no loops in locking.
     */
    private fun reorderLock(users: Pair<User, User>): Pair<User, User> {
        if (users.first.id > users.second.id) {
            return Pair(users.first, users.second)
        }
        return Pair(users.second, users.first)
    }
}
