package com.revolut.service

import com.github.kittinunf.result.Result
import com.revolut.model.User
import java.util.*

/**
 * User service
 */
interface UserService {
    /**
     * Finds user by given id
     * @param id - id of user to find
     * @return result with user in it
     */
    fun getUser(id: UUID): Result<User, Exception>
}
