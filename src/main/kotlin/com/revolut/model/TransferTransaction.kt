package com.revolut.model

import java.math.BigDecimal
import java.util.*

/**
 * Transfer transaction class
 * @param srcId - who transfers money
 * @param destId - who receives money
 * @param amount - amount of money to transfer
 */
open class TransferTransaction(
    val srcId: UUID,
    val destId: UUID,
    val amount: BigDecimal
) {
    /**
     * Checks if transaction is valid
     */
    fun isValid() = srcId != destId
            && amount > BigDecimal.ZERO

    override fun toString(): String {
        return "TransferTransaction(srcId=$srcId, destId=$destId, amount=$amount)"
    }


}

/**
 * Poison transaction that is used to stop queue processor
 */
class PoisonTransaction : TransferTransaction(UUID.randomUUID(), UUID.randomUUID(), BigDecimal.ONE)
