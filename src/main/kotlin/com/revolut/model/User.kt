package com.revolut.model

import java.math.BigDecimal
import java.util.*

/**
 * User data class
 * @param id - id of user
 * @param name - name of user
 * @param email - email of user
 * @param balance - current balance
 */
data class User(
    val id: UUID,
    val name: String,
    val email: String,
    var balance: BigDecimal
) {
    /**
     * Simulates user locking
     * @param logic - logic to execute inside lock
     * @return locked user
     */
    fun lockUser(logic: () -> Unit): User {
        synchronized(this) {
            logic()
            return this
        }
    }
}
