package com.revolut.processor

import com.revolut.model.PoisonTransaction
import com.revolut.model.TransferTransaction
import com.revolut.service.TransferService
import mu.KLogging
import java.util.concurrent.BlockingQueue

/**
 * Runnable that handles incoming transfer transactions
 * @param transferQueue - queue of transactions
 * @param transferService - service that commits transactions
 */
class TransferProcessorRunnable(
    private val transferQueue: BlockingQueue<TransferTransaction>,
    private val transferService: TransferService
) : Runnable {

    override fun run() {
        logger.info { "Start transfer processor" }
        while (!Thread.currentThread().isInterrupted) {
            val transferTransaction = transferQueue.take()
            if (transferTransaction is PoisonTransaction) {
                logger.info { "Got poison transaction. Stop processing." }
                break
            }
            transferService.commitTransfer(transferTransaction)
                .fold(
                    { logger.info { "Transaction $transferTransaction has been committed" } },
                    { ex -> logger.error("Cannot commit transaction $transferTransaction", ex) })
        }
    }

    /**
     * Logger
     */
    companion object : KLogging()
}
