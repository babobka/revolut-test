package com.revolut.rest

import com.revolut.di.kodein
import com.revolut.model.TransferTransaction
import com.revolut.processor.TransferProcessorRunnable
import com.revolut.service.UserService
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.kodein.di.generic.instance
import java.util.*
import java.util.concurrent.BlockingQueue

//TODO write tests
fun main(args: Array<String>) {
    val userService by kodein.instance<UserService>()
    val transferQueue by kodein.instance<BlockingQueue<TransferTransaction>>()
    val transferProcessorRunnable by kodein.instance<TransferProcessorRunnable>()
    Thread(transferProcessorRunnable).start()

    val server = embeddedServer(Netty, port = 8080) {
        //TODO what is ContentNegotiation
        install(ContentNegotiation) {
            //TODO How it works?
            gson {
                setPrettyPrinting()
            }
        }
        routing {
            get("/user/{userId}") {
                val uuid = UUID.fromString(call.parameters["userId"])
                userService.getUser(uuid).fold({ user ->
                    call.respond(user)
                }, { ex ->
                    //TODO Why 404 in case if respond was not called?
                    call.respondText(ex.message!!, status = HttpStatusCode.InternalServerError)
                })
            }
            post("/transfer") {
                //TODO how it works?
                val transferTransaction = call.receive(TransferTransaction::class)
                if (transferTransaction.isValid()) {
                    transferQueue.put(transferTransaction)
                    call.respondText { "Transaction has been added to queue" }
                } else {
                    call.respondText("Invalid transaction", status = HttpStatusCode.BadRequest)
                }
            }
        }
    }
    //TODO why wait?
    server.start(wait = true)
}
