package com.revolut.repository

import com.revolut.model.User
import mu.KLogging
import java.util.*

/**
 * Dummy user repository implementation
 * @param initialUserData - list of users to store
 */
class DummyUserRepository(initialUserData: List<User>) : UserRepository {

    private val users = HashMap<UUID, User>()

    init {
        logger.info { "Initial data" + initialUserData.map { user -> "\n$user" } }
        initialUserData.forEach { user ->
            users[user.id] = user
        }
    }

    override fun get(id: UUID) = users[id]

    override fun getAll() = users.values

    /**
     * Logger
     */
    companion object : KLogging()
}
