package com.revolut.repository

import com.revolut.model.User
import java.util.*

/**
 * User repository. Read only operations.
 */
interface UserRepository {

    /**
     * Returns user by id
     * @param id - user id to find
     * @return user if user with given [id] is present or null otherwise
     */
    fun get(id: UUID): User?

    /**
     * Returns all the users
     */
    fun getAll(): Collection<User>
}
