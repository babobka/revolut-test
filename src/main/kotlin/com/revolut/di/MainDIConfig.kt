package com.revolut.di

import com.revolut.model.TransferTransaction
import com.revolut.model.User
import com.revolut.processor.TransferProcessorRunnable
import com.revolut.repository.DummyUserRepository
import com.revolut.service.TransferService
import com.revolut.service.TransferServiceImpl
import com.revolut.service.UserService
import com.revolut.service.UserServiceImpl
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * This file contains the main DI configurations.
 * All the dependencies are stored in [kodein] val
 */
private const val MAX_TRANSACTIONS_COUNT = 1024

val kodein = Kodein {

    // Mock user repository
    bind<DummyUserRepository>() with singleton {
        DummyUserRepository(
            listOf(
                User(UUID.randomUUID(), "Shrek", "shrek@swamp.com", BigDecimal(10)),
                User(UUID.randomUUID(), "Donkey", "donkey@swamp.com", BigDecimal(0)),
                User(UUID.randomUUID(), "Fiona", "fiona@swamp.com", BigDecimal(25_000)),
                User(UUID.randomUUID(), "Lord Farquaad", "farquaad@kingdom.com", BigDecimal(100_500)),
                User(UUID.randomUUID(), "Puss in Boots", "puss@swamp.com", BigDecimal(5))
            )
        )
    }

    // Service to work with users(read only operations)
    bind<UserService>() with singleton { UserServiceImpl(instance()) }

    // Service to work with transfers
    bind<TransferService>() with singleton { TransferServiceImpl(instance()) }

    /**
     * Queue that is used to handle transfer transactions.
     * Transaction stored in this queue will be processed later
     * */
    bind<BlockingQueue<TransferTransaction>>() with singleton {
        LinkedBlockingQueue<TransferTransaction>(
            MAX_TRANSACTIONS_COUNT
        )
    }

    // Runnable that handles transactions from previously declared queue
    bind<TransferProcessorRunnable>() with singleton { TransferProcessorRunnable(instance(), instance()) }
}
